MANIFESTO FOR AGILE SOFTWARE DEVELOPMENT

Principles behind the Agile Manifesto

We follow these principles:
--Julia Shtohryn


1.Individuals and interactions over processes and tools
--Vita Durnyak

2. Welcome changing requirements, even late in 
development. Agile processes harness change for 
the customer's competitive advantage.
--Helen Maherovska 

3. Deliver working software frequently, from a 
couple of weeks to a couple of months, with a 
preference to the shorter timescale.
--Viktoria Bilous

4.Responding to change over following a plan
--Andriy Arterchuk

5.Build projects around motivated individuals.
Give them the environment and support they need,
and trust them to get the job done. 
--Mykhailo Shtohryn

6. The most efficient and effective method of conveying 
information to and within a development team is face-to-face 
conversation. 
--Skrypnyk Dmytro

7.Working software is the primary measure of progress.
--Vita Durnyak

8.Agile processes promote sustainable development. 
The sponsors, developers, and users should be able 
to maintain a constant pace indefinitely. 
--Helen Maherovska  

9. Continuous attention to technical excellence 
and good design enhances agility.
--Viktoria Bilous

10.Simplicity--the art of maximizing the amount
of work not done--is essential. 
--Andriy Arterchuk

11.The best architectures, requirements, and designs
emerge from self-organizing teams. 
--Mykhailo Shtohryn

12. At regular intervals, the team reflects on how to become 
more effective, then tunes and adjusts its behavior accordingly. 
--Skrypnyk Dmytro

13. Main rulles:
	1) Indidviduals and interactions over processes and tools
 	2) Working software over comprehensive documentation
	3) Customer collaboration over contract negotitation
	4) Responding to change over following plan
--Yarema Kleputs

THE END
--Julia Shtohryn
